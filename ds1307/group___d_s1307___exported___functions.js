var group___d_s1307___exported___functions =
[
    [ "DS1307 configuration functions", "group___d_s1307___conf__func.html", "group___d_s1307___conf__func" ],
    [ "DS1307 control functions", "group___d_s1307___ctrl__func.html", "group___d_s1307___ctrl__func" ],
    [ "DS1307 date&time functions", "group___d_s1307___time__func.html", "group___d_s1307___time__func" ],
    [ "DS1307 GPIO functions", "group___d_s1307___gpio__func.html", "group___d_s1307___gpio__func" ],
    [ "DS1307 Square-Wave functions", "group___d_s1307___sqw__func.html", "group___d_s1307___sqw__func" ],
    [ "DS1307 battery-backed RAM functions", "group___d_s1307___ram__func.html", "group___d_s1307___ram__func" ],
    [ "DS1307 logging functions", "group___d_s1307___log__func.html", "group___d_s1307___log__func" ],
    [ "DS1307 version functions", "group___d_s1307___ver__func.html", "group___d_s1307___ver__func" ]
];