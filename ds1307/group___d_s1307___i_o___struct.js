var group___d_s1307___i_o___struct =
[
    [ "ds1307_io_s", "structds1307__io__s.html", [
      [ "handle", "structds1307__io__s.html#a81011b79683fab64ce3aff71114f8fdd", null ],
      [ "read", "structds1307__io__s.html#ab491c8b27a1acf5b4af9c3e6fdcebe27", null ],
      [ "write", "structds1307__io__s.html#a0a2b0e66d0934740c4c44041272eb4f0", null ]
    ] ],
    [ "ds1307_io_t", "group___d_s1307___i_o___struct.html#gaceb654c207d4009280c0040345ff2400", null ]
];