var group___d_s1307___conf__func =
[
    [ "ds1307_close", "group___d_s1307___conf__func.html#gad0f88b886a35bd5e23e970c6eeca8101", null ],
    [ "ds1307_open", "group___d_s1307___conf__func.html#gaad2e5293217012d57e428d17943ad418", null ],
    [ "ds1307_register_io", "group___d_s1307___conf__func.html#gae383bf1f5e84f13a8b8d0b68d94dc54e", null ],
    [ "ds1307_register_settings", "group___d_s1307___conf__func.html#ga3a140844f18b67752132e2050349d8ca", null ],
    [ "ds1307_set_default_settings", "group___d_s1307___conf__func.html#ga33d1f154b12b1b8f3ab61dc656f353d5", null ]
];