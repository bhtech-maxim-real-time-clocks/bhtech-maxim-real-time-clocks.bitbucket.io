var group___d_s1307___date___time___struct =
[
    [ "ds1307_date_s", "structds1307__date__s.html", [
      [ "day", "structds1307__date__s.html#a72369a1087b2aeffe374bb054cb97c12", null ],
      [ "dow", "structds1307__date__s.html#aabddf0d22b7031d058852bd83b5e281c", null ],
      [ "month", "structds1307__date__s.html#a3e00faf7fbf9805e9ec4d2edd6339050", null ],
      [ "year", "structds1307__date__s.html#a7af2065789bc84419b8d5fe109be83b5", null ]
    ] ],
    [ "ds1307_time_s", "structds1307__time__s.html", [
      [ "ampm", "structds1307__time__s.html#a6957e7ab067aa30b44fba3be6a6f2238", null ],
      [ "hours", "structds1307__time__s.html#a00a531a34a1d603329df5778f1203ab6", null ],
      [ "minutes", "structds1307__time__s.html#a7acca8be0094a19be6e308ac05924c4f", null ],
      [ "mode", "structds1307__time__s.html#a37e90f5e3bd99fac2021fb3a326607d4", null ],
      [ "seconds", "structds1307__time__s.html#a46729a903be1a03cdb248fb48d84d4f5", null ]
    ] ],
    [ "ds1307_date_t", "group___d_s1307___date___time___struct.html#ga24b0a78e65b4201e6b32520b61f59892", null ],
    [ "ds1307_time_t", "group___d_s1307___date___time___struct.html#ga5dc79e182d9d49cec32ac6b3efe140df", null ]
];