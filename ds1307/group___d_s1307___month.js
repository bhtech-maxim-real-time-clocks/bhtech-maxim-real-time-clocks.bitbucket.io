var group___d_s1307___month =
[
    [ "DS1307_APRIL", "group___d_s1307___month.html#ga59d6a61d12d15f5ebdffe061262f0bda", null ],
    [ "DS1307_AUGUST", "group___d_s1307___month.html#gaceb8dcac9cf1dae8cf9314c38483793c", null ],
    [ "DS1307_DECEMBER", "group___d_s1307___month.html#ga17e44ce222835e75562f87c5f7656faa", null ],
    [ "DS1307_FEBRUARY", "group___d_s1307___month.html#ga09b0cb9a2ebed3b667703d00d93b3270", null ],
    [ "DS1307_JANUARY", "group___d_s1307___month.html#ga1823d3c503c1ce92a09f54335616f0ac", null ],
    [ "DS1307_JULY", "group___d_s1307___month.html#ga729c8afa702a1d864abc768fb1e844b8", null ],
    [ "DS1307_JUNE", "group___d_s1307___month.html#ga0ccecd8a534eceecf88c5ebbcf63b1a4", null ],
    [ "DS1307_MARCH", "group___d_s1307___month.html#gaecedad94ac4f8397b68bdf6eba6d4eb9", null ],
    [ "DS1307_MAY", "group___d_s1307___month.html#gaf6c7afa32ea08c38b9e58275a858ecc4", null ],
    [ "DS1307_NOVEMBER", "group___d_s1307___month.html#ga55d5749c512cf58ae0fcebf22656fbf2", null ],
    [ "DS1307_OCTOBER", "group___d_s1307___month.html#gaf5e22a583c59233b5a123636f3ee4986", null ],
    [ "DS1307_SEPTEMBER", "group___d_s1307___month.html#ga81c43e091f27862871ec954c9739a19e", null ]
];