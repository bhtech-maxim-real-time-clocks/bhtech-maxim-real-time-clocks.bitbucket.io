var group___d_s1307___error___codes =
[
    [ "DS1307_E_COMM_FAIL", "group___d_s1307___error___codes.html#gac06b836d54e3f4c1f8c17542e0977601", null ],
    [ "DS1307_E_DEV_NOT_FOUND", "group___d_s1307___error___codes.html#ga6558ffa65b946a94e9b59c147be851e5", null ],
    [ "DS1307_E_INVALID_LEN", "group___d_s1307___error___codes.html#gac08d4e8f6adf13f372fd814d5dace9fa", null ],
    [ "DS1307_E_INVALID_VAL", "group___d_s1307___error___codes.html#ga4592710c592b8961f89fce873e1d6fb4", null ],
    [ "DS1307_E_NULL_PTR", "group___d_s1307___error___codes.html#ga81fb118e45735549e6cd00c7fa781150", null ],
    [ "DS1307_E_SLEEP_MODE_FAIL", "group___d_s1307___error___codes.html#ga50f0db35f8c0234ffff0c6f41dd6baa2", null ],
    [ "DS1307_OK", "group___d_s1307___error___codes.html#ga77e076b8e6b258c1fa6679ad87dde747", null ]
];