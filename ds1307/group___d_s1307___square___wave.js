var group___d_s1307___square___wave =
[
    [ "DS1307_SQW_1_HZ", "group___d_s1307___square___wave.html#ga0d211bfcac3d181b232a53009be7dcca", null ],
    [ "DS1307_SQW_32_768_KHZ", "group___d_s1307___square___wave.html#ga482b5ea05257d27b27fd9040ff9dcb69", null ],
    [ "DS1307_SQW_4_096_KHZ", "group___d_s1307___square___wave.html#ga7c86f2b6d3cd4ecf99c5c9c536ef83cd", null ],
    [ "DS1307_SQW_8_192_KHZ", "group___d_s1307___square___wave.html#ga0b95cd13d075dfa215b03b0a21038287", null ]
];