var group___d_s1307___exported___types =
[
    [ "DS1307 functions pointers", "group___d_s1307___f_u_n_c___ptr.html", "group___d_s1307___f_u_n_c___ptr" ],
    [ "DS1307 IO structure", "group___d_s1307___i_o___struct.html", "group___d_s1307___i_o___struct" ],
    [ "DS1307 date&time structure", "group___d_s1307___date___time___struct.html", "group___d_s1307___date___time___struct" ],
    [ "DS1307 settings structure", "group___d_s1307___settings___struct.html", "group___d_s1307___settings___struct" ],
    [ "DS1307 device structure", "group___d_s1307___dev___struct.html", "group___d_s1307___dev___struct" ]
];