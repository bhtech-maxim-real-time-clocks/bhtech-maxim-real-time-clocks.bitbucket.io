var modules =
[
    [ "DS1307 Common Macros", "group___d_s1307__common__macros.html", "group___d_s1307__common__macros" ],
    [ "DS1307 Common Parameters", "group___d_s1307__common__params.html", "group___d_s1307__common__params" ],
    [ "DS1307 Exported Constants", "group___d_s1307___exported___constants.html", "group___d_s1307___exported___constants" ],
    [ "DS1307 Exported Types", "group___d_s1307___exported___types.html", "group___d_s1307___exported___types" ],
    [ "DS1307 Exported Functions", "group___d_s1307___exported___functions.html", "group___d_s1307___exported___functions" ]
];