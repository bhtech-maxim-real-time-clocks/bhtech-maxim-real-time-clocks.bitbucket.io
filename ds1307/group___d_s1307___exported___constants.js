var group___d_s1307___exported___constants =
[
    [ "DS1307 Slave Address", "group___d_s1307___slave___address.html", "group___d_s1307___slave___address" ],
    [ "DS1307 Error Codes", "group___d_s1307___error___codes.html", "group___d_s1307___error___codes" ],
    [ "DS1307 Hour Mode", "group___d_s1307___hour___mode.html", "group___d_s1307___hour___mode" ],
    [ "DS1307 AM/PM", "group___d_s1307___a_m___p_m.html", "group___d_s1307___a_m___p_m" ],
    [ "DS1307 day of week", "group___d_s1307___day___of___week.html", "group___d_s1307___day___of___week" ],
    [ "DS1307 month", "group___d_s1307___month.html", "group___d_s1307___month" ],
    [ "DS1307 square wave frequency", "group___d_s1307___square___wave.html", "group___d_s1307___square___wave" ],
    [ "DS1307 operation mode selection", "group___d_s1307___o_p___mode.html", "group___d_s1307___o_p___mode" ],
    [ "DS1307 output pin state", "group___d_s1307___pin___state.html", "group___d_s1307___pin___state" ]
];