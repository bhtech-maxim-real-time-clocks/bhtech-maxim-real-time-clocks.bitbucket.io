/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Maxim DS1307 Real-Time Clock", "index.html", [
    [ "DS1307 rtc API", "index.html", [
      [ "Introduction", "index.html#autotoc_md1", null ],
      [ "Integration details", "index.html#autotoc_md3", null ],
      [ "File information", "index.html#autotoc_md5", null ],
      [ "Supported rtc interfaces", "index.html#autotoc_md7", null ],
      [ "Usage guide", "index.html#autotoc_md9", [
        [ "Initializing the rtc", "index.html#autotoc_md10", [
          [ "Example for I2C", "index.html#autotoc_md11", null ]
        ] ],
        [ "Templates for function pointers", "index.html#autotoc_md12", null ],
        [ "Suspending and Resuming the rtc device", "index.html#autotoc_md13", null ],
        [ "Getting and Setting the rtc date&time info", "index.html#autotoc_md14", null ],
        [ "Setting the rtc output pin level", "index.html#autotoc_md15", null ],
        [ "Enabling the square-wave to a particular frequency", "index.html#autotoc_md16", null ],
        [ "Reading and Writing from and to the battery-backed RAM", "index.html#autotoc_md17", null ],
        [ "Enabling and Customizing the debug logging API", "index.html#autotoc_md18", null ],
        [ "Getting the rtc driver version", "index.html#autotoc_md19", null ]
      ] ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';